import { Component } from '@angular/core';
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  nodeCount: number;
  nodeCountArr = [];
  array = [];
  graf = [];
  sonuc = [];
  uzunluk;

  constructor() {}

  onCalculate() {
    for (let i = 0; i < this.nodeCount * this.nodeCount; i++) {
      this.array.push('input'+i);
    }
    for (let i = 0; i < this.nodeCount; i++) {
      this.nodeCountArr.push('');
    }
  }

  run() {
    // console.log('asdas: ' + $('#00').val())
    let k = 0;
    for (let i = 0; i < this.nodeCount; i++) {
      this.graf.push([]);
    }

    for (let i = 0; i < this.nodeCount; i++) {
      for (let j = 0; j < this.nodeCount; j++) {
        this.graf[i][j] = $('#'+i+''+j).val();
        k++;
      }
    }

    this.floydWarshall();
  }

  floydWarshall() {
    this.uzunluk = this.graf.length;
    this.graf.forEach(element => {
      this.sonuc.push([]);
    });
    for (let i = 0; i < this.uzunluk; i++)
      for (let j = 0; j < this.uzunluk; j++)
        this.sonuc[i][j] = this.graf[i][j];

    for (let k = 0; k < this.uzunluk; k++) {
      for (let i = 0; i < this.uzunluk; i++) {
        for (let j = 0; j < this.uzunluk; j++) {
          if (parseInt(this.sonuc[i][k]) + parseInt(this.sonuc[k][j]) < parseInt(this.sonuc[i][j]))
            this.sonuc[i][j] = parseInt(this.sonuc[i][k]) + parseInt(this.sonuc[k][j]);
        }
      }
    }

    console.log('asdas: ' + JSON.stringify(parseInt(this.graf[0][0]) + parseInt(this.graf[0][1])));
  }
}
